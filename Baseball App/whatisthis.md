#### What is this?

This is a small app I'm building in Laravel to help me manage my son's baseball teams (I have two sons that play) 

Ultimate goal will be text message, email reminders to parents for practices and games (so I don't have to do that). 

Also, in it's fully finalized state, it will be a stat tracking app for youth baseball. Maybe one day I'll package it up as a SaaS but for now, it's something that is "scratching my own itch"

What you'll see: 

A bit from the API that I'm building, nothing fancy, just showing how it confirms to the [JSON-API standards](http://jsonapi.org/), uses traits for some repeated code, keeping controllers only to REST verbs, and Fractal to help transform the DB result into something that's publically consumable. (Currently there is no query caching, since I'm still in development)

You can see all the code for this project here: [Bitbucket Repo](https://bitbucket.org/systemseven/baseball-team-manager/src/909f24a371a2?at=master)