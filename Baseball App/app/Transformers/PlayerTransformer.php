<?php

namespace App\Transformers;

use Carbon\Carbon;
use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class PlayerTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var $resource
     * @return array
     */
    public function transform($resource)
    {
        $dob = new Carbon($resource->dob);
        return [
            'id' => (int) $resource->id,
            'parent_id' => $resource->user_id,
            'team_id' => $resource->team_id,
            'firstname' => $resource->firstname,
            'lastname' => $resource->lastname,
            'fullname' => $resource->firstname.' '.$resource->lastname,
            'photo' => $resource->photo,
            'jersey'=>$resource->jersey,
            'bats'=>$resource->bats,
            'fields'=>$resource->fields,
            'age'=> $dob->age,
            'created_at' => $resource->created_at,
            'updated_at' => $resource->updated_at
			
        ];
    }
}
