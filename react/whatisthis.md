#### What is This?

This is a small app that I'm rebuilding in React for a freelance client. It was started the week of 1/12 which explains why it's "incomplete" and unstyled. 

The focus here is making sure that I'm following React best practices with Container and Presentational components and using a proper data flow. 

Redux will be added in as I move further into the application. 