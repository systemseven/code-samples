import React from 'react';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';

// Layouts
import UnauthLayout from './components/layouts/unauth-layout';

// Pages
import AuthenticateContainer from './components/containers/authenticate-container';
import ChooseGuestContainer from './components/containers/choose-guest-container';
import RegisterGuestContainer from './components/containers/register-guest-container';

export default (
  <Router history={browserHistory}>
    <Route component={UnauthLayout}>
      <Route path="/" component={AuthenticateContainer} />
      <Route path="choose-guest/:family" component={ChooseGuestContainer} />
      <Route path="register" component={RegisterGuestContainer} />
    </Route>
  </Router>
);