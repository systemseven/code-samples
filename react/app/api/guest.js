import axios from 'axios';

const API_URL = 'https://api.carriganfarms.com';
const API_VER = 'v1';

export function findByCellphone(number)
{    
    let uri = `${API_URL}/${API_VER}/families?phone=${number.replace(/ /g,'')}`;
    return axios.get(uri)
        .then(response => response.data);
}

export function getFamilyMembers(familyId)
{    
    let uri = `${API_URL}/${API_VER}/families/${familyId}/guests`;
    return axios.get(uri)
        .then(response => response.data);
}