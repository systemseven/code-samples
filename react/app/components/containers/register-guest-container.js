import React from 'react';
import * as GuestAPI from '../../api/guest';
import RegisterGuestForm from '../views/register-guest-form';
import GuestWaiver from '../views/guest-waiver';
import { browserHistory } from 'react-router';

const RegisterGuestContainer = React.createClass({

  getInitialState: function() {
    return {
      loading: true,
      showWaiver: false,
      needsVest: false
    }
  },

  render: function() {
    return (
     <div id="register-guest-container">
      <RegisterGuestForm />
      {this.state.showWaiver ? <GuestWaiver needsVest={this.state.needsVest} /> : null }
     </div>
    );
  }

});

export default RegisterGuestContainer;