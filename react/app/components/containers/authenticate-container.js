import React from 'react';
import AuthenticateGuest from '../views/authenticate-guest';
import AuthenticateStaff from '../views/authenticate-staff';
import * as GuestAPI from '../../api/guest';
import { browserHistory } from 'react-router';

const AuthenticateContainer = React.createClass({

  getInitialState: function() {
    return {
      cellphone: '',
      email: '',
      password: '',
      loading: false
    }
  },

  handleCellphoneChange: function(e) {
    this.setState({cellphone: e.target.value});
  },

  handleCellphoneSubmit: function(){
    if(this.state.cellphone.length >= 10){
      this.setState({loading: true});
      GuestAPI.findByCellphone(this.state.cellphone).then(result => {
        this.setState({loading: false});
        browserHistory.push(`/choose-guest/${result.data.id}`);
      }).catch(error => {
        browserHistory.push('/register');
      });
    }
  },

  render: function() {
    return (
     <div id="authenticate">
      <AuthenticateGuest 
        loading={this.state.loading} 
        handleCellphoneChange={this.handleCellphoneChange} 
        handleCellphoneSubmit={this.handleCellphoneSubmit}
        />
      <AuthenticateStaff />
     </div>
    );
  }

});

export default AuthenticateContainer;