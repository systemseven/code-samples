import React from 'react';
import * as GuestAPI from '../../api/guest';
import ChooseGuestList from '../views/choose-guest-list';
import Loader from '../views/loader';
import GuestWaiver from '../views/guest-waiver';
import { browserHistory, Link } from 'react-router';

const ChooseGuestContainer = React.createClass({

  getInitialState: function() {
    return {
      loading: true,
      family: null,
      showWaiver: false,
      needsVest: false,
      selectedGuest: null,
      guests: []
    }
  },

  componentDidMount: function() {    
    GuestAPI.getFamilyMembers(this.props.params.family).then(result => {
      if(result.data.length){
        this.setState({guests: result.data, family: this.props.params.family, loading: false})
      }else{
        browserHistory.push('/register');
      }
      
    });
  },

  selectGuest: function(guestId, age){
    if(age < 12){
      this.setState({needsVest: true});
    }
    this.setState({selectedGuest: guestId, showEventList: true});
  },

  render: function() {
    return (
     <div id="choose-guest-list">
      <p>Made a mistake? <Link to="/">Start Over</Link></p>
      {this.state.loading ? <Loader /> : <ChooseGuestList guests={this.state.guests} selectGuest={this.selectGuest}/>}
      {this.state.showWaiver ? <GuestWaiver needsVest={this.state.needsVest} /> : null }
     </div>
    );
  }

});

export default ChooseGuestContainer;