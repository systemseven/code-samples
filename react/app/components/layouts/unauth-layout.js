import React from 'react';
import { Link } from 'react-router';
import UnauthHeader from '../views/unauth-header';

// Using "Stateless Functional Components"
export default function(props) {
  return (
    <div className="app">
      <UnauthHeader />
      <section className="unauth-layout">
        {props.children}
      </section>
    </div>
    );
}