import React from 'react';
import { Link } from 'react-router';

// "Stateless Functional Component"
export default function(props) {
  return (
     <header className="primary-header">
        <h1>Carrigan Farms, LLC | Swim Test App</h1>
        <nav>
            <ul>
            <li><Link to="/" activeClassName="active">Home</Link></li>
            <li><Link to="/users" activeClassName="active">Users</Link></li>
            </ul>
        </nav>
      </header>
  );
}