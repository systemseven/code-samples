import React from 'react';
import { Link } from 'react-router';

// "Stateless Functional Components"
export default function(props) {
  return (
    <div className="data-list">

      {props.guests.map(guest => {

        return (
          <div key={guest.id} className="data-list-item">
            <div className="details">
              <div className="avatar">
                <img src={guest.photos.small} />
              </div>
              {guest.full_name}
            </div>
            <div className="controls">
              <button onClick={props.selectGuest.bind(null, guest.id, guest.age)}>This is Me!</button>
            </div>
          </div>
        );

      })}
      <Link to="/register">I'm not listed, Add Me!</Link>
    </div>
  );
}