import React from 'react';

// "Stateless Functional Components"
export default function(props) {
  return (
    <div className="guest-waiver">
        <h1>Carrigan Farms Event Waiver <br/><small>You must accept to swim in the quarry</small></h1>
        {props.needsVest ? (
            <div>
                <p><strong>Since you aren't twelve (12) years old yet, you can't take the swim test.  You must wear a life jacket at all times when in the water.  Please see a lifeguard if you have questions.</strong></p>
            </div>
        ) : 
        null  
        }   

         <h2>Swim at your own risk.</h2>
         <h2>The water is twenty-five (25) feet deep.</h2>

         <h3>Warning</h3>
         <p>Under North Carolina law, there is no liability for an injury to or death of a participant in an agritourism activity conducted at this agritourism location if such injury or death results from the inherent risks of the agritourism activity. Inherent risks of agritourism activities include, among others, risks of injury inherent to land, equipment, and animals, as well as the potential for you to act in a negligent manner that may contribute to your injury or death. You are assuming the risk of participating in this agritourism activity.</p>

         <h3>Quarry</h3>
         Table of Contents:
         <ul>
         <li>The water is 25 feet deep</li>
         <li>YOU CANNOT TOUCH THE BOTTOM</li>
         <li>There is a risk of drowning</li>
         </ul>
         <p>These risks can be caused by my (in) actions or the (in)actions of others</p>
         <p>I agree to hold harmless Carrigan Farms, LLC, and all affiliated parties specified in this document, for any injury, costs, etc.</p>
         <p>The water in the quarry is approximately twenty-five feet deep. You will not be able to touch the bottom; there is no place in the quarry where you can stand. Consequently, participation in activities, or use of the facilities, at the quarry at Carrigan Farms LLC naturally involves the risk of injury or death, whether the undersigned, someone else, or the natural environment causes it. By signing this, you state that you FULLY UNDERSTAND THAT: (a) QUARRY ACTIVITIES INVOLVE RISKS AND DANGERS OF  SERIOUS BODILY INJURY, INCLUDING BUT NOT LIMITED TO DROWNING, BUMPING INTO ROCKS, WATER IMPACTS FROM JUMPS, KICKING ROCKS, PERMANENT DISABILITY, PARALYSIS, DEATH & MANY OTHER FACTORS (“RISKS”); (b) these risks and dangers may be caused by my own actions or inactions, the actions or inactions of others participating in the activities, or the condition in which the activity takes place; (c) there may be OTHER RISKS AND SOCIAL AND ECONOMIC LOSSES either not known to me or not readily foreseeable at this time. As such, the undersigned agrees that he or she understands and voluntarily accepts this risk and agrees to hold harmless Carrigan Farms, LLC, Chimera Properties, LLC, Douglas Wade Carrigan, Kelly Phylene Carrigan, any of their staff, affiliates, associates, contractors, volunteer staff, or any others associated with Carrigan Farms, LLC for any injury or death, including and without limitation, personal, bodily or mental injury, economic loss or any physical or property damage to the undersigned, the undersigned’s spouse, the undersigned’s children, guest or relative, (hereinafter collectively “GUEST”) resulting from the GUEST’s use of the facilities; and I FULLY ACCEPT AND ASSUME ALL SUCH RISKS AND ALL RESPONSIBILITY FOR LOSSES, COSTS, AND DAMAGES I incur as a result of my voluntary participation or that of the GUEST. Moreover, I understand the effect of the waiver and acceptance of risk on my legal rights.</p>

         <h3>Swim test</h3>
         Table of Contents:
         <ul>
         <li>You must be at least 12 years old to take the swim test</li>
         <li>If you do not pass the swim test you must wear a lifejacket in the Quarry at all times</li>
         <li>The swim test involves various risks that that you voluntarily incur by taking the test</li>
         </ul>
         <p>These risks can be caused by your (in)actions or the (in)actions of others</p>
         <p>In order to swim in the quarry without a lifejacket, you must be at least 12 years old and pass our swim test. If you are under the age of 12, you must wear a lifejacket at all times while swimming in the quarry. There are no exceptions to this rule. The swim test includes jumping off of a 10 foot cliff into the quarry, swimming around 60 feet to a buoy, treading water for 60 seconds (1 minute), and swimming out of the quarry. If we feel that you did not complete the swim test effectively, you must wear a lifejacket at all times while swimming in the quarry.  The swim test assesses endurance, treading, and quality of stroke, among other attributes.  All grades on the swim test are final and re-testing is not permitted on the same day. When taking the swim test, please demonstrate your best swimming. A grade of PASS on the swim test means that person may elect to swim without a life jacket in the quarry.  Those persons who do not want to take the swim test, or who do not pass the swim test, will have to wear a lifejacket at all times while swimming in the quarry. The swim test naturally involves various risks of physical injury or death that can be caused by the undersigned, the environment, or someone else. By signing this waiver, you state that YOU FULLY UNDERSTAND THAT: (a) THE SWIM TEST  INVOLVE RISKS AND DANGERS OF  SERIOUS BODILY INJURY, INCLUDING BUT NOT LIMITED TO DROWNING, BUMPING INTO ROCKS, WATER IMPACTS FROM JUMPS, KICKING ROCKS, PERMANENT DISABILITY, PARALYSIS, DEATH & MANY OTHER FACTORS (“RISKS”); (b) these risks and dangers may be caused by my own actions or inactions, the actions or inactions of others participating in the swim test, or the condition in which the test takes place; (c) there may be OTHER RISKS AND SOCIAL AND ECONOMIC LOSSES either not known to me or not readily foreseeable at this time. As such, the undersigned agrees that he or she understands and voluntarily accepts this risk and agrees to hold harmless Carrigan Farms, LLC, Chimera Properties, LLC, Douglas Wade Carrigan, Kelly Phylene Carrigan, any of their staff, affiliates, associates, contractors, volunteer staff, or any others associated with Carrigan Farms, LLC for any injury or death, including and without limitation, personal, bodily or mental injury, economic loss or any physical or property damage to the undersigned; and I FULLY ACCEPT AND ASSUME ALL SUCH RISKS AND ALL RESPONSIBILITY FOR LOSSES, COSTS, AND DAMAGES I incur as a result of my voluntary participation. Moreover, I understand the effect of the waiver and acceptance of risk on my legal rights.</p>

        <div className="waiverActions">
            <button>I agree, let's go swimming!</button>
            <button>No I do not agree to these terms <br/> (you will not be allowed to swim)</button>
        </div>

    </div>
  );
}