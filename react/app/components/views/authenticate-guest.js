import React from 'react';
import Cleave from 'cleave.js/react';
import CleavePhone from 'cleave.js/dist/addons/cleave-phone.us';

// "Stateless Functional Component"
export default function(props) {
  return (
    <div className="authenticate-guest">
        <div className="guest-login">
            <p>To get started, enter your cellphone number.</p>
            <Cleave placeholder="Cellphone Number..." options={{phone: true, phoneRegionCode: 'US'}} onChange={props.handleCellphoneChange} />
            <button disabled={props.loading} onClick={props.handleCellphoneSubmit}>
                {props.loading ? (
                    <span>Loading...</span>
                ) : 
                (
                    <span>Get Started</span>
                )  
                }                  
            </button>
        </div>
    </div>
  );
}