path = require('path');
const webpack = require('webpack');

module.exports = {
  //uncomment for smaller, but longer production builds
  //devtool: 'cheap-module-source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false
        }
    })    
  ],
  // webpack folder’s entry js — excluded from jekll’s build process.
  entry: './app/entry.js',
  output: {
    // we’re going to put the generated file in the assets folder so jekyll will grab it.
    path: 'src/assets/js/',
    filename: 'bundle.js'
  },
  module: {
  loaders: [
    {
      test: /\.jsx?$/,
      exclude: /node_modules\/(?!cleave.js)/,
      loader: 'babel', 
      query: {
        presets: ['es2015','react','stage-0']
      }
    }
    ]
  }
};