#### What is This?

In my spare time, I run a podcast - American Military History Podcast

This is my attempt at building a little Laravel app that would aggregate all my mentions from Twitter (using the Streaming API), Facebook page posts, and iTunes reviews. 

It would then take each of those results, and run sentiment analysis on them to tell me if all the comments were overally positive, negative or neutral, as well as what the top words used overall, and on each feed

This files are the 'cron jobs' that would run at intervals or kicked off by the app to generate those stats.