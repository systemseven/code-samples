<?php

namespace App\Console\Commands;

use Antoineaugusti\LaravelSentimentAnalysis\Facades\SentimentAnalysis;
use App\FBPost;
use App\Feed;
use Facebook\Facebook;
use Illuminate\Console\Command;

class SyncFacebookFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'castrocket:syncfacebook';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grabs all mentions of Facebook page and analyzes them';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fb = new Facebook();
        $feeds = Feed::where('feed_type','facebook')->get(['feed_type','id','podcastId','user_id']);

        foreach($feeds as $feed){
            try {
                $url = sprintf('/%s/tagged',$feed->podcastId);
                $token = env('FACEBOOK_APP_ID').'|'.env('FACEBOOK_APP_SECRET');
                $response = $fb->get($url,$token);
                $result = $response->getDecodedBody();
                $posts = collect($result['data']);

                $posts->each(function($post,$key) use($feed) {
                    $sentiment = new SentimentAnalysis();
                    $scores = $sentiment::scores($post['message']);
                    $p = new FBPost();
                        $p->user_id = $feed->user_id;
                        $p->fb_id = $post['id'];
                        $p->fb_message = $post['message'];
                        $p->fb_timestamp = $post['tagged_time'];
                        $p->sentiment = $sentiment::score($post['message']);
                        $p->sentiment_decision = $sentiment::decision($post['message']);
                        $p->sentiment_score_positive = $scores['positive'];
                        $p->sentiment_score_neutral = $scores['neutral'];
                        $p->sentiment_score_negative = $scores['negative'];
                    $p->save();
                });


            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                //dd($e->getMessage());
            }
        }

    }
}
