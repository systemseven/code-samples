<?php

namespace App\Console\Commands;

use App\FBPost;
use App\Feed;
use App\Review;
use App\Stat;
use App\Tweet;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class StatsEngine extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'castrocket:statsengine';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a cached version of user statistics and sentiment analysis';
    /*
     * The statistics that will be serialized to JSON
     * and stored in the database
     *
     * @var array
     */
    protected $statistics;

    /*
     * Current user id for each loop
     *
     * @var integer
     */
    protected $currentUser;

    /*
     * Stop words used in filtering most popular words from reviews
     *
     * @var array
     */
    protected $stopwords;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->statistics = [];
        $this->setStopwords();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::get(['id'])->pluck('id')->all();

        foreach($users as $user) {
            $this->statistics=[];
            $this->currentUser = $user;
            $this->generateITunesStats();
            $this->generateTwitterStats();
            $this->generateFacebookStats();

            //once we've done all the others, now let's
            //generate total stats and then write to the DB
            $this->generateTotalStats();

            //if we have an entry for yesterday, we'll have a
            //comparison object
            $this->compareStats();

            $this->saveStats();
        }
    }

    private function generateITunesStats()
    {
        $reviewStats = ['sentiment'=>[],'rating'=>[]];
        $wordString = '';
        $sentimentCounts = ['positive'=>0,'negative'=>0,'neutral'=>0];
        $ratingCounts = ['1star'=>0,'2star'=>0,'3star'=>0,'4star'=>0,'5star'=>0];
        $sentimentPercents = ['positive'=>0,'negative'=>0,'neutral'=>0];
        $totalCounts = 0;
        $feedIds = Feed::where('user_id',$this->currentUser)->where('feed_type','itunes')->get(['id'])->pluck('id')->all();
        foreach($feedIds as $feed) {
            $reviews = Review::where('feed_id',$feed)->get(['title','content','rating','sentiment','sentiment_decision']);
            $reviewStats['sentiment']['avg'] = floatval(collect($reviews->pluck('sentiment')->all())->avg());
            $reviewStats['sentiment']['min'] = floatval(collect($reviews->pluck('sentiment')->all())->min());
            $reviewStats['sentiment']['max'] = floatval(collect($reviews->pluck('sentiment')->all())->max());
            $reviewStats['sentiment']['total'] = collect($reviews->pluck('sentiment')->all())->count();
            $reviewStats['rating']['avg'] = floatval(collect($reviews->pluck('rating')->all())->avg());
            $reviewStats['rating']['min'] = floatval(collect($reviews->pluck('rating')->all())->min());
            $reviewStats['rating']['max'] = floatval(collect($reviews->pluck('rating')->all())->max());
            $reviewStats['rating']['total'] = collect($reviews->pluck('rating')->all())->count();

            collect($reviews->pluck('sentiment_decision')->all())->each(function($item,$key) use (&$sentimentCounts, &$totalCounts){
                $sentimentCounts[$item]++;
                $totalCounts++;
            });

            collect($reviews->pluck('rating')->all())->each(function($item,$key) use (&$ratingCounts){
                $ratingCounts[$item.'star']++;
            });

            if($totalCounts > 0)
            {
                collect($sentimentPercents)->each(function($item,$key) use(&$sentimentPercents, &$sentimentCounts,$totalCounts){
                    $sentimentPercents[$key] = round(($sentimentCounts[$key] / $totalCounts) * 100);
                });
            }

            $reviewStats['sentiment']['counts'] = $sentimentCounts;
            $reviewStats['sentiment']['percent'] = $sentimentPercents;
            $reviewStats['rating']['counts'] = $ratingCounts;

            $counts = collect($reviewStats['sentiment']['counts']);
            $reviewStats['sentiment']['overall'] = $counts->sort()->reverse()->keys()[0];

            $reviews->each(function($item,$key) use (&$wordString){
                $wordString = $wordString. ' ' . $item->title.' '.$item->content;
                $wordString = str_replace(["\r\n", "\r", "\n"], "", $wordString);
            });

            $reviewStats['popularWords'] = $this->extract_common_words(strtolower($wordString));


        }

        $this->statistics['itunes'] = $reviewStats;
    }

    private function generateTwitterStats()
    {
        $tweetStats = [];
        $wordString = '';
        $sentimentCounts = ['positive'=>0,'negative'=>0,'neutral'=>0];
        $sentimentPercents = ['positive'=>0,'negative'=>0,'neutral'=>0];
        $totalCounts = 0;
        $feedIds = Feed::where('user_id',$this->currentUser)->where('feed_type','twitter')->get(['id'])->pluck('id')->all();
        foreach($feedIds as $feed) {
            $tweets = Tweet::where('user_id',$this->currentUser)->get(['tweet_text','sentiment','sentiment_decision']);
            $tweetStats['sentiment']['avg'] = floatval(collect($tweets->pluck('sentiment')->all())->avg());
            $tweetStats['sentiment']['min'] = floatval(collect($tweets->pluck('sentiment')->all())->min());
            $tweetStats['sentiment']['max'] = floatval(collect($tweets->pluck('sentiment')->all())->max());
            $tweetStats['sentiment']['total'] = collect($tweets->pluck('sentiment')->all())->count();

            collect($tweets->pluck('sentiment_decision')->all())->each(function($item,$key) use (&$sentimentCounts,&$totalCounts){
                $sentimentCounts[$item]++;
                $totalCounts++;
            });

            if($totalCounts > 0)
            {
                collect($sentimentPercents)->each(function($item,$key) use(&$sentimentPercents, &$sentimentCounts,$totalCounts){
                    $sentimentPercents[$key] = round(($sentimentCounts[$key] / $totalCounts) * 100);
                });
            }

            $tweetStats['sentiment']['counts']  = $sentimentCounts;
            $tweetStats['sentiment']['percent'] = $sentimentPercents;

            $counts = collect($tweetStats['sentiment']['counts']);
            $tweetStats['sentiment']['overall'] = $counts->sort()->reverse()->keys()[0];

            $tweets->each(function($item,$key) use (&$wordString){
                $wordString = $wordString. ' ' . $item->tweet_text;
                $wordString = str_replace(["\r\n", "\r", "\n"], "", $wordString);
            });

            $tweetStats['popularWords'] = $this->extract_common_words(strtolower($wordString));
        }

        $this->statistics['twitter'] = $tweetStats;
    }

    private function generateFacebookStats()
    {
        $fbStats = [];
        $wordString = '';
        $sentimentCounts = ['positive'=>0,'negative'=>0,'neutral'=>0];
        $sentimentPercents = ['positive'=>0,'negative'=>0,'neutral'=>0];
        $totalCounts = 0;
        $feedIds = Feed::where('user_id',$this->currentUser)->where('feed_type','facebook')->get(['id'])->pluck('id')->all();
        foreach($feedIds as $feed) {
            $posts = FBPost::where('user_id',$this->currentUser)->get(['fb_message','sentiment','sentiment_decision']);
            $fbStats['sentiment']['avg'] = floatval(collect($posts->pluck('sentiment')->all())->avg());
            $fbStats['sentiment']['min'] = floatval(collect($posts->pluck('sentiment')->all())->min());
            $fbStats['sentiment']['max'] = floatval(collect($posts->pluck('sentiment')->all())->max());
            $fbStats['sentiment']['total'] = collect($posts->pluck('sentiment')->all())->count();

            collect($posts->pluck('sentiment_decision')->all())->each(function($item,$key) use (&$sentimentCounts, &$totalCounts, &$totalSentiment){
                $sentimentCounts[$item]++;
                $totalCounts++;
            });

            if($totalCounts > 0)
            {
                collect($sentimentPercents)->each(function($item,$key) use(&$sentimentPercents, &$sentimentCounts,$totalCounts){
                    $sentimentPercents[$key] = round(($sentimentCounts[$key] / $totalCounts) * 100);
                });
            }

            $fbStats['sentiment']['counts']  = $sentimentCounts;
            $fbStats['sentiment']['percent'] = $sentimentPercents;

            $counts = collect($fbStats['sentiment']['counts']);
            $fbStats['sentiment']['overall'] = $counts->sort()->reverse()->keys()[0];

            $posts->each(function($item,$key) use (&$wordString){
                $wordString = $wordString. ' ' . $item->fb_message;
                $wordString = str_replace(["\r\n", "\r", "\n"], "", $wordString);
            });

            $fbStats['popularWords'] = $this->extract_common_words(strtolower($wordString));
        }

        $this->statistics['facebook'] = $fbStats;
    }

    private function generateTotalStats()
    {
        $overAllStats = ['sentiment'=>[]];
        $statCollection = collect($this->statistics);
        $sentimentPercents = ['positive'=>0,'negative'=>0,'neutral'=>0];
        $totalCounts = 0;
        //filter out the zero values to give a truer average
        $nonZero = $statCollection->pluck('sentiment.avg')->filter(function($value,$key){
            return $value > 0;
        });

        $overAllStats['sentiment']['avg'] = floatval($nonZero->avg());
        $overAllStats['sentiment']['min'] = floatval($statCollection->pluck('sentiment.min')->avg());
        $overAllStats['sentiment']['max'] = floatval($statCollection->pluck('sentiment.max')->avg());
        $overAllStats['sentiment']['counts']['positive'] = $statCollection->pluck('sentiment.counts.positive')->sum();
        $overAllStats['sentiment']['counts']['neutral'] = $statCollection->pluck('sentiment.counts.neutral')->sum();
        $overAllStats['sentiment']['counts']['negative'] = $statCollection->pluck('sentiment.counts.negative')->sum();

        $counts = collect($overAllStats['sentiment']['counts']);
        $overAllStats['sentiment']['overall'] = $counts->sort()->reverse()->keys()[0];

        foreach($overAllStats['sentiment']['counts'] as $cnt)
        {
            $totalCounts = $totalCounts + $cnt;
        }
        if($totalCounts > 0)
        {
            $sentimentPercents['positive'] = round(($overAllStats['sentiment']['counts']['positive'] / $totalCounts) * 100);
            $sentimentPercents['neutral'] = round(($overAllStats['sentiment']['counts']['neutral'] / $totalCounts) * 100);
            $sentimentPercents['negative'] = round(($overAllStats['sentiment']['counts']['negative'] / $totalCounts) * 100);
        }

        $overAllStats['sentiment']['percent'] = $sentimentPercents;

        $allWords = collect($statCollection->pluck('popularWords'));
        $overAllWords = [];
        foreach($allWords->all() as $wordGroup){
            if(count($wordGroup) && $wordGroup != null) {
                foreach ($wordGroup as $key => $value) {
                    if (isset($overAllWords[$key])) {
                        $overAllWords[$key] = $overAllWords[$key] + $value;
                    } else {
                        $overAllWords[$key] = $value;
                    }
                }
            }
        }
        arsort($overAllWords);

        $overAllStats['popularWords'] = array_slice($overAllWords, 0, 6);
        $this->statistics['overall'] = $overAllStats;
    }

    private function compareStats(){
        $from = Carbon::now();
        $to = Carbon::now();
        $statRecord = Stat::where('user_id' , $this->currentUser)->whereBetween('created_at',[$from->startOfDay(),$to->endOfDay()])->get();
        if($statRecord->count())
        {
            $yesterday = json_decode($statRecord[0]->stats);
            $current = json_decode(json_encode($this->statistics));

            $changes = [];
            $currItunes = $current->itunes;
            $prevItunes = $yesterday->itunes;
            $currTwitter = $current->twitter;
            $prevTwitter = $yesterday->twitter;
            $currFB = $current->facebook;
            $prevFB = $yesterday->facebook;

            if($currItunes->rating->avg > $prevItunes->rating->avg)
            {
                $changes['itunes']['ratings']['desc'] = 'moved up from';
                $changes['itunes']['ratings']['value'] = $prevItunes->rating->avg;
            }
            if($currItunes->rating->avg < $prevItunes->rating->avg)
            {
                $changes['itunes']['ratings']['desc'] = 'moved down from';
                $changes['itunes']['ratings']['value'] = $prevItunes->rating->avg;
            }
            if($current->overall->sentiment->overall != $yesterday->overall->sentiment->overall)
            {
                $changes['overall']['sentiment'] = $yesterday->overall->sentiment->overall;
            }

            $changes['itunes']['sentiment']['positive'] = $currItunes->sentiment->counts->positive - $prevItunes->sentiment->counts->positive;
            $changes['itunes']['sentiment']['neutral'] = $currItunes->sentiment->counts->neutral - $prevItunes->sentiment->counts->neutral;
            $changes['itunes']['sentiment']['negative'] = $currItunes->sentiment->counts->negative - $prevItunes->sentiment->counts->negative;
            $changes['twitter']['sentiment']['positive'] = $currTwitter->sentiment->counts->positive - $prevTwitter->sentiment->counts->positive;
            $changes['twitter']['sentiment']['neutral'] = $currTwitter->sentiment->counts->neutral - $prevTwitter->sentiment->counts->neutral;
            $changes['twitter']['sentiment']['negative'] = $currTwitter->sentiment->counts->negative - $prevTwitter->sentiment->counts->negative;
            $changes['facebook']['sentiment']['positive'] = $currFB->sentiment->counts->positive - $prevFB->sentiment->counts->positive;
            $changes['facebook']['sentiment']['neutral'] = $currFB->sentiment->counts->neutral - $prevFB->sentiment->counts->neutral;
            $changes['facebook']['sentiment']['negative'] = $currFB->sentiment->counts->negative - $prevFB->sentiment->counts->negative;

            $this->statistics['trend'] = $changes;
        }else{
            $this->statistics['trend'] = false;
        }
    }

    private function saveStats()
    {
        $from = Carbon::now();
        $to = Carbon::now();
        $statRecord = Stat::where('user_id' , $this->currentUser)->whereBetween('created_at',[$from->startOfDay(),$to->endOfDay()])->get();
        if($statRecord->count()) {
            $statUpdate = Stat::find($statRecord[0]->id);
            $statUpdate->stats = json_encode($this->statistics);
            $statUpdate->save();
        }else{
            $newStat = new Stat();
                $newStat->user_id = $this->currentUser;
                $newStat->stats = json_encode($this->statistics);
            $newStat->save();
        }
    }

    private function setStopwords()
    {
        $this->stopwords = ["great","podcast","thats","a", "about", "above", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also","although","always","am","among", "amongst", "amoungst", "amount",  "an", "and", "another", "any","anyhow","anyone","anything","anyway", "anywhere", "are", "around", "as",  "at", "back","be","became", "because","become","becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom","but", "by", "call", "can", "cannot", "cant", "co", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven","else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itself", "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own","part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thickv", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself", "yourselves", "the"];
    }

    private function extract_common_words($string, $max_count = 7) {
        $stop_words = $this->stopwords;
        $string = preg_replace('/ss+/i', '', $string);
        $string = trim($string); // trim the string
        $string = preg_replace('/[^a-zA-Z -]/', '', $string); // only take alphabet characters, but keep the spaces and dashes too…
        $string = strtolower($string); // make it lowercase

        preg_match_all('/\b.*?\b/i', $string, $match_words);
        $match_words = $match_words[0];

        foreach ( $match_words as $key => $item ) {
            if ( $item == '' || in_array(strtolower($item), $stop_words) || strlen($item) <= 3 ) {
                unset($match_words[$key]);
            }
        }

        $word_count = str_word_count( implode(" ", $match_words) , 1);
        $frequency = array_count_values($word_count);
        arsort($frequency);

        //arsort($word_count_arr);
        $keywords = array_slice($frequency, 0, $max_count);
        return $keywords;
    }
}
