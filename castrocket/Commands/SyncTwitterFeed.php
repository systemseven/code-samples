<?php

namespace App\Console\Commands;

use App\Feed;
use App\TwitterStream;
use Illuminate\Console\Command;

class SyncTwitterFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'castrocket:synctwitter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Connects to Streaming Twitter API to pull down mentions';
    protected $twitterStream;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TwitterStream $twitterStream)
    {
        $this->twitterStream = $twitterStream;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $twitter_consumer_key = env('TWITTER_CONSUMER_KEY', '');
        $twitter_consumer_secret = env('TWITTER_CONSUMER_SECRET', '');

        $feeds = Feed::where('feed_type','twitter')->get(['name']);
        $twitterHandles = $feeds->pluck('name')->all();

        $this->twitterStream->consumerKey = $twitter_consumer_key;
        $this->twitterStream->consumerSecret = $twitter_consumer_secret;
        $this->twitterStream->setTrack($twitterHandles);
        $this->twitterStream->consume();
    }
}
