<?php

namespace App\Console\Commands;

use Antoineaugusti\LaravelSentimentAnalysis\Facades\SentimentAnalysis;
use App\Feed;
use App\Review;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class SyncFeedReviews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'castrocket:syncreviews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Syncs the reviews for each podcast between local and iTunes';

    /**
     * The reviews collection object pulled from DB
     *
     * @var  Collection */
    private $reviews;

    /**
     * Meta information about the podcast
     *
     * @var Array
     */
    private $meta;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->reviews = collect();
        $this->meta = [];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $feeds = Feed::where('feed_type','itunes')->get(['feed_type','id','podcastId']);

        $feeds->each(function($item,$key){
            $page = 1;

            //fetchReviews is recursive and will fire until
            //it has fetched all reviews
            $this->fetchReviews($item,$page);

            //updates the podcasts meta info with recent iTunes data
            $this->updateMetaInfo($item->id, $this->meta);

            //saves the reviews to the database
            $this->saveReviews($item->id);

            //reset the reviews collection for the next loop
            $this->reviews = collect();

        });
    }

    private function fetchReviews($item,$page)
    {
        if($page == 1) {
            $urlBase = 'https://itunes.apple.com/rss/customerreviews/id=%s/json';
            $reviewsUrl = sprintf($urlBase,$item->podcastId);
        }else {
            $urlBase = 'https://itunes.apple.com/rss/customerreviews/id=%s/sortby=mostrecent/page=%s/json';
            $reviewsUrl = sprintf($urlBase, $item->podcastId, $page);
        }

        $httpClient = new Client();
        $request = $httpClient->request('GET', $reviewsUrl);
        $result = str_replace('im:rating','imRating',$request->getBody());
        $result = str_replace('im:releaseDate','imReleaseDate',$result);
        $result = str_replace('im:name','imName',$result);
        $result = str_replace('im:image','imImage',$result);
        $result = json_decode($result);

        if($page == 1) {
            //only need to set the meta information the first loop through
            $this->meta = $result->feed->entry[0];
        }

        foreach($result->feed->entry as $entry)
        {
            if(isset($entry->content)) {
                //print_r($entry->content);
                $this->reviews->push($entry);
            }
        }

        $lastPage = $this->parsePageNumbers($result->feed->link);

        //increment current page
        $page++;
        if($page <= $lastPage){
            $this->fetchReviews($item,$page);
        }else{
            return true;
        }

    }

    private function parsePageNumbers($links)
    {
        //don't want a global match here, itunes
        //also holds the current page, so we
        //jsut need to get the first
        $re = '/page=[0-9]/';

        foreach($links as $link)
        {
           if($link->attributes->rel == 'last'){
               preg_match($re, $link->attributes->href, $matches);
               return intval(str_replace('page=','',$matches[0]));
           }
        }
    }

    private function updateMetaInfo($feed,$meta){
        $feed = Feed::find($feed);
        $feed->thumbnail = $meta->imImage[2]->label;
        $feed->summary = $meta->summary->label;
        $feed->name = $meta->imName->label;
        $feed->category = $meta->category->attributes->label;
        $feed->releaseDate = $meta->imReleaseDate->attributes->label;
        return $feed->save();
    }

    private function saveReviews($feed)
    {
        //we're going to grab the itunes ids of all the reviews this feed has
        //if there's a difference, we'll inject ONLY the difference
        //and queue up the notifications
        $currentIds = [];
        $currentReviews = Review::where('feed_id',$feed)->get(['itunes_id']);
        foreach($currentReviews as $review){
            array_push($currentIds,$review->itunes_id);
        }
        $feedIds = $this->reviews->pluck('id.label')->all();

        $feedCollection = collect($feedIds);
        $databaseCollection = collect($currentIds);

        $diff = $feedCollection->diff($databaseCollection);
        $newReviews = $diff->all();

        $this->reviews->each(function($item,$key) use ($feed,$newReviews){
            if(in_array($item->id->label,$newReviews)) {
                $sentiment = new SentimentAnalysis();
                $reviewText = $item->title->label.' '.$item->content->label;
                $scores = $sentiment::scores($reviewText);

                $review = new Review;
                    $review->feed_id = $feed;
                    $review->itunes_id = $item->id->label;
                    $review->author = $item->author->name->label;
                    $review->title = $item->title->label;
                    $review->content = $item->content->label;
                    $review->rating = $item->imRating->label;
                    $review->sentiment = $sentiment::score($reviewText);
                    $review->sentiment_decision = $sentiment::decision($reviewText);
                    $review->sentiment_score_positive = $scores['positive'];
                    $review->sentiment_score_neutral = $scores['neutral'];
                    $review->sentiment_score_negative = $scores['negative'];
                return $review->save();

                //TODO: kick off notifications
            }
        });
    }
}
